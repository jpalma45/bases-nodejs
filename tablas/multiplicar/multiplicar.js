const fs = require('fs');
const colors  = require('colors');

let data = '';

let listarTabla = (base, limite = 10) => {

    return new Promise((resolve, reject) => {
        if ( !Number(base) ) {
            reject(`${base}. No es un numero`);
            return;
        }

        if (!Number(limite)) {
            reject(`${limite}. No es un numero`)
        }

        console.log('==================================='.red);
        console.log(`Tabla de ${ base }`.green);
        console.log('==================================='.red);

        for (let index = 0; index <= limite; index++) {
            data += `${ base } * ${index} = ${ base * index} \n`
        }
        reject(data);
        data = '';
    });

}


let createArchivo = (base, limite = 10) => {

    return new Promise((resolve, reject) => {

        if ( !Number(base) ) {
            reject(`${base}. No es un numero`);
            return;
        }

        for (let index = 0; index <= limite; index++) {
            data += `${ base } * ${index} = ${ base * index} \n`
        }
        
        fs.writeFile(`./tablas/tabla-${ base }-al-${ limite }.txt`, data, (err) =>{
            if (err) reject(err);
            else 
                resolve(`tabla-${ base }-al-${ limite }.txt`.green)
        });
    });
}

module.exports = {
    createArchivo,
    listarTabla
}