const argv = require('./config/yargs').argv;
const colors  = require('colors/safe');

const { createArchivo, listarTabla } = require('./tablas/multiplicar/multiplicar');

let command = argv._[0];

switch( command ) {
    case 'listar': 
        listarTabla(argv.base, argv.limite).then(response => {
            console.log(response);
        }).catch(err => console.log(err));
    break;

    case 'crear':
        createArchivo( argv.base, argv.limite ).then( response => {
            console.log(`Archivo creado:`, colors.green(response));
        }).catch(err => console.log(err));
    break;

    default:
        console.log('comando no reconocido');
        break;
}

